﻿namespace MameCloneRemover
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.IO;
	using System.Xml.Linq;

	public class Program
	{
		public static void Main(string[] Args)
		{
			// check arguments
			if (Args.Length == 0) _Quit("Usage: MameNoClone <mame 0.78 path>");

			// get the path
			var MamePath = Path.GetFullPath(Args[0]);

			// check the path
			if (!Directory.Exists(MamePath)) _Quit($"Mame path '{MamePath}' not found");

			// look for the roms directory
			var RomDirectory = MamePath + Path.DirectorySeparatorChar + "roms" + Path.DirectorySeparatorChar;
			if (!Directory.Exists(RomDirectory)) _Quit($"Mame path '{MamePath}' does not contain roms");

			// load the mame dat file
			XElement MameDat = null;
			try
			{
				MameDat = XElement.Load(MamePath + Path.DirectorySeparatorChar + "MAME 078.dat");  
			}
			catch (Exception E)
			{
				_Quit($"Error while reeading Mame 078.dat file: {E.Message}");
			}

			// look for the clones
			Console.WriteLine("Looking for clones...");

			// ReSharper disable once PossibleNullReferenceException
			var Clones = 
			(from G in MameDat.Descendants("game")
				let Game = G.Attributes()
				where Game.Any(_ => _.Name == "cloneof")
				let Name = Game.First(_ => _.Name == "name").Value
				let CloneOf = Game.First(_ => _.Name == "cloneof").Value
				where File.Exists(RomDirectory + Name + ".zip")
				select new KeyValuePair<string, string>(Name, CloneOf)).ToDictionary(_ => _.Key, _ => _.Value);

			Console.WriteLine($"{Clones.Count} clones found");

			// if there are clones found, move them
			if (Clones.Count > 0)
			{
				// create the clone directory
				var CloneDirectory = MamePath + Path.DirectorySeparatorChar + "clones" + Path.DirectorySeparatorChar;
				if (!Directory.Exists(CloneDirectory)) Directory.CreateDirectory(CloneDirectory);

				// move the clones
				foreach (var Clone in Clones)
				{
					var Filename = Clone.Key + ".zip";
					Console.WriteLine($"Moving {Filename}, clone of {Clone.Value}");
					File.Move(RomDirectory + Filename, CloneDirectory + Filename);
				}
			}

			void _Quit(string Message)
			{
				Console.WriteLine(Message);
				Environment.Exit(-1);
			}
		}
	}
}
